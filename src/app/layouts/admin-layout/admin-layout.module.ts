import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { PointsComponent } from '../../pages/points/points.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { MembershipComponent } from '../../pages/membership/membership.component'
import { RegisterComponent } from '../../pages/register/register.component'
import { UserRegisterComponent } from '../../pages/user-register/user-register.component';
import { FilterPipe } from '../../pipes/filter.pipe'
import { MembershipPipe } from '../../pipes/membership.pipe'
import { AdminPipe } from '../../pipes/admin.pipe'
import { StatusPipe } from '../../pipes/status.pipe'
import { OwnerPipe } from '../../pipes/owner.pipe'
import { PublicComponent } from '../../pages/public/public.component'
import { PublicService } from '../../pages/public/public.service'
import { NormalMemberComponent } from 'src/app/pages/normal-member/normal-member.component';
import { NormalMemberService } from 'src/app/pages/normal-member/normal-member.service';
import { PlusMemberComponent } from 'src/app/pages/plus-member/plus-member.component';
import { PlusMemberService } from 'src/app/pages/plus-member/plus-member.service';
import { FreelanceMemberComponent } from 'src/app/pages/freelance-member/freelance-member.component';
import { FreelanceMemberService } from 'src/app/pages/freelance-member/freelance-member.service';
import { LoginService } from 'src/app/pages/login/login.service';
import { CustomerComponent } from 'src/app/pages/customer/customer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    NgxPaginationModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    PointsComponent,
    MembershipComponent,
    RegisterComponent,
    UserRegisterComponent,
    FilterPipe,
    MembershipPipe,
    AdminPipe,
    StatusPipe,
    OwnerPipe,
    PublicComponent,
    NormalMemberComponent,
    PlusMemberComponent,
    FreelanceMemberComponent,
    CustomerComponent
  ],
  providers:[PublicService,NormalMemberService,PlusMemberService,FreelanceMemberService,LoginService]
})

export class AdminLayoutModule { }
