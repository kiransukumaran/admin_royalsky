import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
// import { IconsComponent } from '../../pages/icons/icons.component';
// import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { PointsComponent } from '../../pages/points/points.component';
import { MembershipComponent } from '../../pages/membership/membership.component'
import { RegisterComponent } from '../../pages/register/register.component'
import { UserRegisterComponent } from '../../pages/user-register/user-register.component';
import { PublicComponent } from 'src/app/pages/public/public.component';
import { NormalMemberComponent } from 'src/app/pages/normal-member/normal-member.component';
import { PlusMemberComponent } from 'src/app/pages/plus-member/plus-member.component';
import { FreelanceMemberComponent } from 'src/app/pages/freelance-member/freelance-member.component';
import { CustomerComponent } from 'src/app/pages/customer/customer.component';


export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'user-profile', component: UserProfileComponent },
  { path: 'points', component: PointsComponent },
  { path: 'membership', component: MembershipComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'user', component: UserRegisterComponent },
  { path: 'public', component: PublicComponent },
  { path: 'normal', component: NormalMemberComponent },
  { path: 'plus', component: PlusMemberComponent },
  { path: 'freelance', component: FreelanceMemberComponent },
  { path: 'customers', component: CustomerComponent },
];
