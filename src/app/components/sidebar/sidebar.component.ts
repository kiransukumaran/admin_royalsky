import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/user-profile', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
  { path: '/public', title: 'Public - Unassigned', icon: 'ni-single-02 text-yellow', class: '' },
  { path: '/points', title: 'Points', icon: 'ni-bullet-list-67 text-red', class: '' },
  { path: '/membership', title: 'Membership', icon: 'ni-key-25 text-info', class: '' },
  { path: '/normal', title: 'Normal Customers', icon: 'ni-key-25 text-info', class: '' },
  { path: '/plus', title: 'Plus Customers', icon: 'ni-key-25 text-info', class: '' },
  { path: '/freelance', title: 'Freelance Customers', icon: 'ni-key-25 text-info', class: '' },
  { path: '/customers', title: 'Customers Summary', icon: 'ni-key-25 text-info', class: '' },
  { path: '/register', title: 'Admin Management', icon: 'ni-circle-08 text-pink', class: '' },
  { path: '/user', title: 'User Management', icon: 'ni-circle-08 text-blue', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }
}
