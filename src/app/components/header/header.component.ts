import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  Nuser: any = 0;
  Puser: any = 0;
  Fuser: any = 0;
  data: any = [];
  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit() {
    this.httpClient.get('http://13.59.192.179:5000/api/register').subscribe(data => {
      this.data = data;
      for (var i = 0; i < this.data.length; i++) {
        if (this.data[i].membershiptype == 1) {
          this.Nuser = Number(this.Nuser) + 1
        }
        else if (this.data[i].membershiptype == 2) {
          this.Puser = Number(this.Puser) + 1
        }
        else {
          this.Fuser = Number(this.Fuser) + 1
        }
      }
    })
  }

}
