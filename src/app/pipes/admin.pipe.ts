import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'admin'
})
export class AdminPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        return items.filter(data => {
            return data.firstname.toLowerCase().includes(searchText) || data.email.toLowerCase().includes(searchText) ||
                data.lastname.toLowerCase().includes(searchText)
        })
    }
}