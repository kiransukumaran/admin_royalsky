import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'membership'
})
export class MembershipPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        if (searchText == "0") {
            return items;
        } else {
            return items.filter(data => {
                return data.membershiptype == Number(searchText)
            })
        }
    }
}