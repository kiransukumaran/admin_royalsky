import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'status'
})
export class StatusPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        if (searchText == "All") {
            return items;
        } else {
            return items.filter(data => {
                return data.membership.status == searchText
            })
        }
    }
}