import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'owner'
})

export class OwnerPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        const email = sessionStorage.getItem('email');
        if (sessionStorage.getItem('role') == 'admin' || sessionStorage.getItem('role') == 'superadmin') {
            return items
        } else {
            return items.filter(data => {
                return data.membership.owner_name == email

            })
        }
    }
}