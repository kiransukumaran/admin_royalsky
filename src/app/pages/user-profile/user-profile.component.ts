import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserProfileService } from './user-profile.service';
import { ExcelService } from 'src/app/excel.service';
import { PublicService } from '../public/public.service';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  Nuser: any = 0;
  Puser: any = 0;
  Fuser: any = 0;
  data: any = [];
  datas: any = [];
  user: any = [];
  member: any = [];
  type: any = [];
  status: string;
  term: String;
  p: number = 1;
  filterType: any = "0"
  report: any = [];
  reportData: any = [];
  scopeData: any = [];
  userRole: any = [];
  owner: string;
  employee: any;
  userType: string;


  constructor(private httpClient: HttpClient, private router: Router, public toastr: ToastrManager, private serv: UserProfileService, private excel: ExcelService, private pubService: PublicService) { }

  ngOnInit() {
    this.userType = sessionStorage.getItem('email')
    this.userRole = sessionStorage.getItem('role');
    if (sessionStorage.getItem('IsloggedIn') == "true") {
      if (this.userRole == 'admin' || this.userRole == 'superadmin') {
        this.pubService.getAllEmployee().subscribe(data => {
          this.employee = data;
        });
      }
      this.serv.getData().subscribe(data => {
        this.data = data;
        this.datas = data;
      })
    } else {
      this.router.navigate(['/login'])
    }
  }

  setCustomerId(id, type, status, i) {

    sessionStorage.setItem("id", id);
    sessionStorage.setItem("i", i);
    this.type = type == 1 ? "Normal" : type == 2 ? "Plus" : "Freelance";
    this.status = status
    var index = sessionStorage.getItem("id");
    this.serv.view(index).subscribe(
      data => {
        this.data = data;
      }
    )
  }

  generate() {

    this.data.forEach(element => {
      this.reportData.push({
        name: element.firstname + " " + element.lastname,
        contactno: element.contactno,
        whatsappno: element.whatsappno,
        email: element.email,
        cardno: element.membership.cardno,
        total: element.membership.total
      })
    });
    this.excel.downloadFile(this.reportData, 'User_profile')
  }


  updateStatus() {
    var index = sessionStorage.getItem("id");

    const mType = this.type == "Normal" ? 1 : this.type == "Plus" ? 2 : 3
    this.serv.updateType(index, mType, this.status).subscribe(
      datas => {
        this.datas = datas
        document.getElementById("memberType" + sessionStorage.getItem("i")).textContent = this.datas.membershiptype == 1 ? 'Normal Member' : this.datas.membershiptype == 2 ? 'Plus Member' : 'Freelance Member'
      }
    )
    document.getElementById("closeBtn").click();
  }

  updateOwner() {
    if (sessionStorage.getItem('role') == 'admin' || sessionStorage.getItem('role') == 'superadmin') {
      const id = sessionStorage.getItem("id")
      this.pubService.assignOwner(id, this.owner).subscribe(data => {
        document.getElementById("ownerBtn").click()
        alert("Ownership updated");
        sessionStorage.removeItem("id")
      })
    } else {
      alert("You are not authorized to do this")
    }
  }

}
