import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  private url = 'http://13.59.192.179:5000/api/'
  constructor(private httpClient: HttpClient) { }

  getData() {
    return this.httpClient.get(this.url + 'register')
  }

  view(index) {
    return this.httpClient.get(this.url + 'register/' + index)
  }

  updateType(index, type, status) {
    return this.httpClient.put(this.url + 'register/' + index, {
      membershiptype: type,
      "membership.status": status,
      "membership.owner_name": sessionStorage.getItem("email"),
      "membership.owner_role": sessionStorage.getItem('role')
    })
  }
  getExcelData() {
    return this.httpClient.get(this.url + 'register/report')
  }
}
