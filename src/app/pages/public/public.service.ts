import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http: HttpClient) { }
  private url = "http://13.59.192.179:5000/api/register/"
  private publicUrl = "http://13.59.192.179:5000/api/membership/public"
  private authUrl = "http://13.59.192.179:5000/api/auth"

  updateOwnerDetails(id) {
    return this.http.put(this.url + id, {
      'membership.owner_name': sessionStorage.getItem('email'),
      'membership.owner_role': sessionStorage.getItem('role')
    })
  }

  getData() {
    return this.http.get(this.publicUrl)
  }

  assignOwner(id, owner) {
    return this.http.put(this.url + id, {
      'membership.owner_name': owner,
      'membership.owner_role': 'staff'
    })
  }

  getAllEmployee() {
    return this.http.get(this.authUrl);
  }

}
