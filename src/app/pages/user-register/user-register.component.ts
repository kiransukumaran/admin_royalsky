import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserRegisterService } from './user-register.service';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit {
  fname: String;
  lname: String;
  email: String;
  dob: String;
  contact: any;
  whatsapp: any;
  type: Number;
  address: String;
  country: String;
  state: String;
  city: String;
  zip: String;
  relStatus: String;
  spousename: String;
  wedDate: String;
  travelStatus: String;
  travelLocn: String;
  res: any;
  data: any = [];
  datas: any = [];
  constructor(private httpClient: HttpClient, private router: Router, public toastr: ToastrManager, private serv: UserRegisterService) { }

  ngOnInit() {
    if (sessionStorage.getItem('IsloggedIn') == 'true') {
      this.serv.getData().subscribe(res => {
        this.res = res
      })
    }
    else {
      this.router.navigate(['/login'])
    }
  }

  register() {

    this.serv.userRegister(this.fname,this.lname,this.contact,this.address,this.dob,this.state,this.country,this.city,this.zip,this.email,this.whatsapp,this.relStatus,this.wedDate,this.spousename,this.type,this.travelLocn,this.travelStatus).subscribe(data => {
      this.data = data;


      if (data["auth"] == true) {
        this.toastr.successToastr("User Registerd and membership details added!!")

        this.router.navigate(['/user-profile'])
      }
    })
  }
}
