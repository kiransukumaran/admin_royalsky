import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRegisterService {
 private url = 'http://13.59.192.179:5000/api/'
  constructor(private httpClient : HttpClient) { }
   
  getData(){
    return this.httpClient.get(this.url + 'register')
  }
  userRegister(fname,lname,contact,address,dob,state,country,city,zip,email,whatsapp,relStatus,wedDate,spousename,type,locn,status){
    return this.httpClient.post(this.url + 'register', {
      firstname: fname,
      lastname: lname,
      contactno: contact,
      address: address,
      password: dob,
      dob: dob,
      state: state,
      country: country,
      city: city,
      zip: zip,
      email: email,
      whatsappno: whatsapp,
      relationshipstatus: relStatus,
      weddingdate: wedDate,
      nameofspouse: spousename,
      membershiptype: type,
      travelLocn: locn,
      travelStatus: status
    })
  }
}
