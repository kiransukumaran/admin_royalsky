import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PointsService {
  private url = 'http://13.59.192.179:5000/api/'
  username: String;
  constructor(private httpClient: HttpClient) { }

  getData() {
    return this.httpClient.get(this.url + 'register')
  }

  addService(id, fulldate, addPoint, addDescription) {
    this.username = sessionStorage.getItem("email")
    return this.httpClient.post(this.url + 'point', {
      userid: id,
      date: fulldate,
      last_added: addPoint,
      description: addDescription,
      refered: this.username,
    })
  }
  deleteService(id, fulldate, deletePoint, deleteDescription) {
    this.username = sessionStorage.getItem("email")
    return this.httpClient.post(this.url + 'point', {
      userid: id,
      date: fulldate,
      last_added: -deletePoint,
      description: deleteDescription,
      refered: this.username,
    })
  }

  getDetails(id) {
    return this.httpClient.get(this.url + 'point/user/' + id)
  }

}
