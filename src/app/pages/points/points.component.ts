import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrManager } from 'ng6-toastr-notifications';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';

import { from } from 'rxjs';
import { jsonpCallbackContext } from '@angular/common/http/src/module';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { PointsService } from './points.service';
import { ExcelService } from '../../excel.service';

@Component({
  selector: 'app-tables',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.scss']
})
export class PointsComponent implements OnInit {
  data: any = [];
  datas: any = [];
  user: any = [];
  total: any = [];
  filter: any = [];
  addPoint: String;
  addDescription: String;
  deletePoint: String;
  deleteDescription: String;
  p: number = 1;
  term: String
  Nuser: any = 0;
  Puser: any = 0;
  Fuser: any = 0;
  userType: string;



  constructor(private httpClient: HttpClient, private router: Router, public toastr: ToastrManager, private serv: PointsService, private excel: ExcelService) { }

  ngOnInit() {
    this.userType = sessionStorage.getItem('email')
    if (sessionStorage.getItem('IsloggedIn') == "true") {
      this.serv.getData().subscribe(
        data => {
          this.data = data;
        }
      )
    }
    else {
      this.router.navigate(['/login'])
    }
  }

  addPoints() {
    var id = sessionStorage.getItem("id");
    var currentDate = new Date();
    var date = currentDate.getDate();
    var month = Number(currentDate.getMonth()) + 1;
    var year = currentDate.getFullYear()
    var fulldate = date + "-" + month + "-" + year;
    this.serv.addService(id, fulldate, this.addPoint, this.addDescription).subscribe(datas => {
      console.log("Added!")
      this.toastr.successToastr('Reward Point added Successfully!!');
      window.location.reload();
    })
    document.getElementById("addBtn").click();

  }

  deletePoints() {
    var id = sessionStorage.getItem("id");
    var currentDate = new Date();
    var date = currentDate.getDate();
    var month = Number(currentDate.getMonth()) + 1;
    var year = currentDate.getFullYear()
    var fulldate = date + "-" + month + "-" + year;
    this.serv.deleteService(id, fulldate, this.deletePoint, this.deleteDescription).subscribe(datas => {
      console.log("Removed!")
      this.toastr.successToastr('Reward Point deleted Successfully!!');
    })
    document.getElementById("deleteBtn").click();
  }

  getHistory(id) {
    // this.user = this.data[i].points
    this.data.forEach(element => {
      if(element._id == id){
        this.user = element.points
      }
    });
  }

  setCustomerId(id) {
    sessionStorage.setItem("id", id);

  }
  //   generate() {
  //     this.excel.downloadFile(this.data, 'Points')
  // }

}
