import { Component, OnInit } from '@angular/core';
import { PublicService } from '../public/public.service';
import { Router } from '@angular/router';
import { CustomerService } from '../customer/customer.service';
import { ExcelService } from 'src/app/excel.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  data: any;
  owner: string;
  userRole: string;
  employee: any;
  filterType: any;
  term: any;
  p:number;
  reportData = new Array();
  userType: any;

  constructor(private pubService: PublicService, private router : Router, private excel: ExcelService, private customerService: CustomerService) { }


  ngOnInit() {
    this.userType = sessionStorage.getItem('email')
    this.userRole = sessionStorage.getItem('role')
    if (sessionStorage.getItem('IsloggedIn') == "true") {
      if (this.userRole == 'admin' || this.userRole == 'superadmin') {
        this.pubService.getAllEmployee().subscribe(data => {
          this.employee = data;
        });
      }
      this.customerService.getData().subscribe(data => {
        this.data = data;
      })
    } else {
      this.router.navigate(['/login'])
    }
  }

  acceptCustomers(id, index) {
    if (this.data[index].membership.owner_name == 'public') {
      this.pubService.updateOwnerDetails(id).subscribe(data => {
        alert("Ownership updated please see your customer list");
        this.router.navigate(['/user-profile'])
      })
    } else {
      alert("This customer is assigned to another staff / please contact admin for changes")
    }
  }

  updateOwner() {
    if (sessionStorage.getItem('role') == 'admin' || sessionStorage.getItem('role') == 'superadmin') {
      const id = sessionStorage.getItem("id")
      this.pubService.assignOwner(id, this.owner).subscribe(data => {
        document.getElementById("ownerBtn").click()
        alert("Ownership updated");
        sessionStorage.removeItem("id")
      })
    } else {
      alert("You are not authorized to do this")
    }

  }

  setCustomerId(id) {
    sessionStorage.setItem("id", id);
  }

  generate() {

    this.data.forEach(element => {

        this.reportData.push({
          name: element.firstname + " " + element.lastname,
          contactno: element.contactno,
          whatsappno: element.whatsappno,
          email: element.email,
          cardno: element.membership.cardno,
          total: element.membership.total
        })
      
    });
    
    if(this.reportData == null || this.reportData == undefined || this.reportData.length == 0){
      alert("Nothing to export")
    } else {

      this.excel.downloadFile(this.reportData, 'Freelance_Member')
    }
  }
}
