import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlusMemberService {

  private url = "http://13.59.192.179:5000/api/membership/type/2"

  constructor(private httpclient : HttpClient) { }

  getData(){
    return this.httpclient.get(this.url)
  }
}
