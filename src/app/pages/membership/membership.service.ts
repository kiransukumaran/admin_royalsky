import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReturnStatement } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class MembershipService {
  private url = "http://13.59.192.179:5000/api/register/"
  constructor(private httpClient: HttpClient) { }

  getMembership() {
    return this.httpClient.get(this.url)
  }

}
