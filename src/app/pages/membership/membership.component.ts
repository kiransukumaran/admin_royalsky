import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { MembershipService } from './membership.service';
import { ExcelService } from '../../excel.service';

@Component({
    selector: 'app-membership',
    templateUrl: './membership.component.html',
    styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {
    Nuser: any = 0;
    Puser: any = 0;
    Fuser: any = 0;
    data: any = [];
    datas: any = [];
    term: String;
    p: number = 1;
    filterStatus: any = "All"
    reportData: any = [];
    userType: string;



    constructor(private httpClient: HttpClient, private router: Router, private serv: MembershipService, private excel: ExcelService) { }

    ngOnInit() {
        this.userType = sessionStorage.getItem('email')

        if (sessionStorage.getItem('IsloggedIn') == "true") {
            this.serv.getMembership().subscribe(
                datas => {
                    this.datas = datas;
                }
            )
        }
        else {
            this.router.navigate(['/login'])
        }
    }

    generate() {
        this.datas.forEach(element => {
            this.reportData.push({
                name: element.firstname + " " + element.lastname,
                contactno: element.contactno,
                whatsappno: element.whatsappno,
                email: element.email,
                cardno: element.membership.cardno,
                total: element.membership.total,
                issuedate: element.membership.issuedate,
                validto: element.membership.validto,
                status: element.membership.status
            })
        });
        this.excel.downloadFile(this.reportData, 'Membership')
    }


}
