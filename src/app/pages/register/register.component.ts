import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { RegisterService } from './register.service';
import { ExcelService } from 'src/app/excel.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  Nuser: any = 0;
  Puser: any = 0;
  Fuser: any = 0;
  data: any = [];
  user: any = [];
  staff: any = [];
  datas: any = [];
  statdata: any = [];
  fname: String;
  lname: String;
  email: String;
  password: String;
  role: String;
  term: String;
  adminRole: String;
  p: number = 1;
  constructor(private httpClient: HttpClient, private router: Router, public toastr: ToastrManager, private serv: RegisterService, private excel: ExcelService) { }

  ngOnInit() {

    this.adminRole = sessionStorage.getItem('role');
    if( this.adminRole == 'staff' && sessionStorage.getItem('IsloggedIn') == "true") {
      alert("You are not authorized to view this page")
      this.router.navigate(['/dashboard'])
    } else if (sessionStorage.getItem('IsloggedIn') == "true") {
      this.serv.getData().subscribe(user => {
        this.user = user;
      })
      this.serv.getStaffs().subscribe(staff => {
        this.staff = staff;
      })
    }
    else {
      this.router.navigate(['/login'])
    }
  }

  register() {

    this.serv.register(this.fname, this.lname, this.password, this.email, this.role).subscribe(datas => {
      this.datas = datas;
      this.toastr.successToastr("Registration Successful!")
      window.location.reload();
    })
  }
  generate() {
    this.excel.downloadFile(this.user, 'adminlist')
  }

  activate(id) {
    this.serv.activateService(id).subscribe(
      statdata => {
        this.statdata = statdata;
        window.location.reload();
      }
    )
  }

  suspend(id) {
    this.serv.suspendService(id).subscribe(
      statdata => {
        this.statdata = statdata;
        window.location.reload();
      }
    )
  }
}
