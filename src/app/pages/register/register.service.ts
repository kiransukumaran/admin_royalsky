import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private url = 'http://13.59.192.179:5000/api/'
  constructor(private httpClient: HttpClient) { }

  getData() {
    return this.httpClient.get(this.url + 'auth/admin')
  }
  getStaffs() {
    return this.httpClient.get(this.url + 'auth/staff')
  }

  register(fname, lname, password, email, role) {
    return this.httpClient.post(this.url + 'auth/register', {
      firstname: fname,
      lastname: lname,
      password: password,
      email: email,
      role: role
    })
  }
  activateService(id) {
    return this.httpClient.put(this.url + 'auth/' + id, {
      status: 1
    })
  }

  suspendService(id) {
    return this.httpClient.put(this.url + 'auth/' + id, {
      status: 0
    })
  }
}
