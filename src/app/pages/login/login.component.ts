import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  email: string;
  password: string
  datas: any = [];

  constructor(private httpClient: HttpClient, private router: Router, public toastr: ToastrManager, private serv: LoginService) { }

  ngOnInit() {
    if (sessionStorage.getItem('IsloggedIn') == 'true') {
      this.router.navigate(['/dashboard'])
    }
  }
  ngOnDestroy() {
  }
  login() {

    this.serv.login(this.email, this.password).subscribe(

      datas => {

        if (datas["status"] == 200) {

          if (datas["data"]["value"]["role"] == "superadmin") {
            sessionStorage.setItem('IsloggedIn', "true");
            sessionStorage.setItem('role', "superadmin");
            sessionStorage.setItem('email', datas["data"]["value"]["email"]);
            sessionStorage.setItem('name', datas["data"]["value"]["firstname"]);
            this.router.navigate(['/dashboard'])
          }
          else if (datas["data"]["value"]["role"] == "admin" && datas["data"]["value"]["status"] == 1) {
            sessionStorage.setItem('IsloggedIn', "true");
            sessionStorage.setItem('role', "admin");
            sessionStorage.setItem('email', datas["data"]["value"]["email"]);
            sessionStorage.setItem('name', datas["data"]["value"]["firstname"]);
            this.router.navigate(['/dashboard'])
          }
          else if (datas["data"]["value"]["role"] == "staff" && datas["data"]["value"]["status"] == 1) {
            sessionStorage.setItem('IsloggedIn', "true");
            sessionStorage.setItem('role', "staff");
            sessionStorage.setItem('email', datas["data"]["value"]["email"]);
            sessionStorage.setItem('name', datas["data"]["value"]["firstname"]);
            this.router.navigate(['/dashboard'])
          }
          else {
            this.toastr.warningToastr('Please contact superadmin to login');

          }
        }
        else {
          this.toastr.warningToastr('Invalid username or password');
        }
      }
    )
  }
}
