import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private Url = " http://13.59.192.179:5000/api/auth/login"
  constructor(private httpClient: HttpClient) { }

  login(email, password) {
    return this.httpClient.post(this.Url, {

      email: email,
      password: password

    })
  }
}
